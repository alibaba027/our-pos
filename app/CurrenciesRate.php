<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

use DB;

class CurrenciesRate extends Model
{
    protected $guarded = ['id'];
    protected $table="currencies_rate";
    public function currency()
    {
    	return $this->belongsTo('App\Currency', 'currency_id');
    }
  }
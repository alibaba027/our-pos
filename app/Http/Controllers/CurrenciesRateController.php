<?php

namespace App\Http\Controllers;

use App\Barcode;
 use App\Currency;
use Illuminate\Http\Request;
use Datatables;
use App\CurrenciesRate;
use App\Product;
use App\Variation;

class CurrenciesRateController extends Controller
{
	public function index()
	{
		$currencies = Currency::all();
		$currenciesRates=CurrenciesRate::all();
        // dd($currenciesRates);
        return view('currenciesRate.index',compact('currencies','currenciesRates'));
	}
	public function save(Request $request)
	{
		$alreadyExist=CurrenciesRate::where('currency_id',$request->currency_id)->first();
		if($alreadyExist == null){
			 CurrenciesRate::Create([
			 	'currency_id'=>$request->currency_id,
			 	'rate'=>$request->rate

			 ]);
				$output = array('success' => 1, 
                            'msg' => __("Successfully Added Currency Rate.")
                        );
				return redirect('/currencies-rate')->with('status', $output);

		}else{
			$output = array('success' => 0, 
                            'msg' => __("This currency rate already exist.")
                        );
			return redirect('/currencies-rate')->with('status', $output);
		}
	}
	 public function destroy($id)
    {

        if (request()->ajax()) {

            try {


                $Currency = CurrenciesRate::findOrFail($id);
                $Currency->delete();

                $output = array('success' => true, 
                            'msg' => __("category.deleted_success"),
                            'id'=>$id
                            );

            } catch(\Exception $e){
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
                $output = array('success' => false, 
                            'msg' => __("messages.something_went_wrong"),
                            'id'=>$id
                        );
            }

            return $output;
        }
    }
    public function edit(Request $request)
    {
        $data = CurrenciesRate::find($request->id);
        return $data;
    }
    public function update(Request $request)
    {
        $currency = CurrenciesRate::find($request->edit_currency_rate);
        $currency->currency_id=$request->currency_id;
         $currency->rate=$request->rate;
         $currency->save();
         $products =Product::where('product_currency',$request->currency_id)->get();
         if(count($products)){
            foreach ($products as $key => $product) {
                # code...
                if($product->type == 'single'){
                    $variation = Variation::where('product_id',$product->id)->first();
                    $variation->default_purchase_price = $variation->foreign_price *$request->rate;
                    $variation->dpp_inc_tax = $variation->foreign_price *$request->rate;
                    $variation->default_sell_price =($variation->foreign_price *$request->rate)+(($variation->profit_percent/100)*($variation->foreign_price *$request->rate));
                    $variation->save();
                }
            }
         }
         // Variation::find();
         $output = array('success' => true, 
                            'msg' => "Successfully updated and all product related this currency also updated",
                        );
      return redirect('/currencies-rate')->with('status',$output);
    }
    public function loadCurrencyAjax(Request $request)
    {
        $currency=CurrenciesRate::where('currency_id',$request->id)->first();
        return $currency;
    }
}

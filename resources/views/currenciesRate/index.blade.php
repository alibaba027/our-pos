@extends('layouts.app')
@section('title', 'Currencies Rate')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Currencies Rate
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <input type="hidden" value="" id="contact_type">

	<div class="box">
        <div class="box-header">
        	<h3 class="box-title">All Currencies Rates</h3>
            	<div class="box-tools">
                    <button type="button" data-toggle="modal" data-target="#create" class="btn btn-block btn-primary">
                    <i class="fa fa-plus"></i> @lang('messages.add')</button>
                </div>
        </div>
        <div class="box-body">
           <div class="table-responsive">
        	<table class="table table-bordered table-striped" id="caurrencyrate_table">
                 
        		<thead>
                   
                        <tr>
                             <td>Sr#</td>
                             <td>Currency</td>
                             <td>Rate</td>
                             <td>Actions</td>
                        </tr>
                    
        		</thead>
                <tbody>
                    @if(count($currenciesRates))
                        @foreach($currenciesRates as $key=>$currency)
                             <tr id="row-{{$currency->id}}">
                                 <td>{{$key+1}}</td>
                                 <td>{{$currency->currency->country}}-{{$currency->currency->currency}}({{$currency->currency->code}})</td>
                                 <td>{{$currency->rate}}</td>
                                 <td><a href="#" class="btn btn-xs btn-primary edit_currency_rate" data-id="{{$currency->id}}"><i class="glyphicon glyphicon-edit"></i>Edit</a>
                                    &nbsp;<button data-href="{{action('CurrenciesRateController@destroy', [$currency->id])}}" class="btn btn-xs btn-danger delete_currencyRate_button" data-di="{{$currency->id}}"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</button></td>
                            </tr>
                            @endforeach
                    @else
                    <p><center>No data found...</center></p>
                     @endif
                </tbody>
        	</table>
           
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
@include('currenciesRate.create')
@include('currenciesRate.edit')
@endsection

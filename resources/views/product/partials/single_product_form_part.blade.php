@if(!session('business.enable_price_tax')) 
  @php
    $default = 0;
    $class = 'hide';
  @endphp
@else
  @php
    $default = null;
    $class = '';
  @endphp
@endif

<div class="col-sm-9"><br>
  <div class="table-responsive">
    <table class="table table-bordered add-product-price-table table-condensed {{$class}}">
        <tr>
          <th>Foreign Price</th>
          <th>@lang('product.default_purchase_price')</th>
          <th>@lang('product.profit_percent') @show_tooltip(__('tooltip.profit_percent'))</th>
          <th>@lang('product.default_selling_price')</th>

        </tr>
        <tr>
          <td>
            <div class="col-sm-12">
             <label>Price </label>
              <input type="text" name="foreign_price" id="foreignPrice" style="width: 100%" class="form-control input-sm dpp input_number" placeholder="Foreign Amount" onkeyup="calculate()">
          </div>
          </td>
          <td>
            <div class="col-sm-6">
              {!! Form::label('single_dpp', trans('product.exc_of_tax') . ':*') !!}

              {!! Form::text('single_dpp', $default, ['class' => 'form-control input-sm dpp input_number', 'placeholder' => 'Excluding Tax', 'required']); !!}
            </div>

            <div class="col-sm-6">
              {!! Form::label('single_dpp_inc_tax', trans('product.inc_of_tax') . ':*') !!}
            
              {!! Form::text('single_dpp_inc_tax', $default, ['class' => 'form-control input-sm dpp_inc_tax input_number', 'placeholder' => 'Including Tax', 'required']); !!}
            </div>
          </td>

          <td>
            <br/>
            {!! Form::text('profit_percent', @num_format($profit_percent), ['class' => 'form-control input-sm input_number', 'id' => 'profit_percent', 'required']); !!}
          </td>

          <td>
            <label><span class="dsp_label">@lang('product.exc_of_tax')</span></label>
            {!! Form::text('single_dsp', $default, ['class' => 'form-control input-sm dsp input_number', 'placeholder' => 'Excluding tax', 'id' => 'single_dsp', 'required']); !!}

            {!! Form::text('single_dsp_inc_tax', $default, ['class' => 'form-control input-sm hide input_number', 'placeholder' => 'Including tax', 'id' => 'single_dsp_inc_tax', 'required']); !!}
          </td>
        </tr>
    </table>
    </div>
</div>
<script type="text/javascript">
  function calculate()
  {
    var Fprice=$('#foreignPrice').val();
    if(Fprice){
      var currency_id=$('#product_currency_foreign').val();
      if(currency_id){
          $.ajax({
                     type: 'post',
                     url: base_url+'load-currency/for-ajax',
                     data: {
                         '_token': $('input[name=_token]').val(),
                         'id':currency_id ,
                        
                     },
                     success: function(data) {
                         if ((data.errors)){
                             $('.error').removeClass('hidden');
                             $('.error').text(data.errors.name);
                         }
                         else {
                             $('.error').addClass('hidden');
                           $('#single_dpp').val(Fprice*data.rate);
                           $('#single_dpp_inc_tax').val(Fprice*data.rate);
                           var percentage = $('#profit_percent').val();
                           var tPrice = ((percentage/100)*(Fprice*data.rate))+(Fprice*data.rate);
                           $('#single_dsp').val(tPrice);
                             console.log(data);

                         }
                     },

                 });
      }else{
        alert("Please select foreign currency");
      }
    }
    
  }
</script>
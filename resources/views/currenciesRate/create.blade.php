
 <style type="text/css">
   .select2-container{
    width: 100% !important;
   }
 </style>

  <!-- Modal -->
  <div class="modal fade" id="create" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Currency Rate</h4>
        </div>
        <form method="post" action="{{url('/add-currency-rate')}}">
       {{ csrf_field() }}
        <div class="modal-body" style="min-height: 150px;">
          <div class="col-md-12">
            <div class="col-md-4">
              <label>Select Currency</label>
            </div>
              <div class="col-sm-8">
                  <div class="form-group">
                      <div class="input-group" style="width: 100%">
                          <span class="input-group-addon" style="width: 20%">
                              <i class="fa fa-money"></i>
                          </span>
                          <select class="form-control select2" name="currency_id" name="currency_id" required="ture">
                            <option value="">--select currency--</option>
                            @foreach($currencies as $currency)
                            <option value="{{$currency->id}}">{{$currency->country}}-{{$currency->currency}}({{$currency->code}})</option>
                            @endforeach
                          </select>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-12">
            <div class="col-md-4">
              <label>Exchange Rate</label>
            </div>
              <div class="col-sm-8">
                  <div class="form-group">
                      <div class="input-group" style="width: 100%">
                          <input type="number" min="0.01" value="1" step="any" name="rate" class="form-control" required="ture">
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary" value="Save">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>
<!--   
</div>

</body>
</html> -->
